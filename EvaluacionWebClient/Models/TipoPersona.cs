﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EvaluacionWeb.Models
{
    [DataContract]
    public class TipoPersona
    {
        [DataMember]
        [Key]
        public string IdTipoPersona { get; set; }

        [DataMember]
        [Display(Name = "Tipo Persona")]
        public string Descripcion { get; set; }

        public virtual ICollection<Cliente> Clientes { get; set; }
    }
}