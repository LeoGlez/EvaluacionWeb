﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EvaluacionWeb.Models
{
    [DataContract]
    public class Contacto
    {
        [DataMember]
        [Key]
        public int IdContacto { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Debes ingresar {0}.")]
        public int IdCliente { get; set; }

        [DataMember]
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Debes ingresar {0}.")]
        [StringLength(50, ErrorMessage = "El campo {0} debe tener tener como máximo {1} caracteres.")]
        public string Nombre { get; set; }

        [DataMember]
        [Display(Name = "Apellido Paterno")]
        [Required(ErrorMessage = "Debes ingresar {0}.")]
        [StringLength(50, ErrorMessage = "El campo {0} debe tener tener como máximo {1} caracteres.")]
        public string ApellidoP { get; set; }

        [DataMember]
        [Display(Name = "Apellido Materno")]
        [Required(ErrorMessage = "Debes ingresar {0}.")]
        [StringLength(50, ErrorMessage = "El campo {0} debe tener tener como máximo {1} caracteres.")]
        public string ApellidoM { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Debes ingresar {0}.")]
        public int IdPuestoTrabajo { get; set; }

        [DataMember]
        [Display(Name = "Teléfono")]
        [Required(ErrorMessage = "Debes ingresar {0}.")]
        [StringLength(20, ErrorMessage = "El campo {0} debe tener tener como máximo {1} caracteres.")]
        [DataType(DataType.PhoneNumber)]
        public string Telefono { get; set; }

        [DataMember]
        [Display(Name = "E-Mail")]
        [Required(ErrorMessage = "Debes ingresar {0}.")]
        [StringLength(30, ErrorMessage = "El campo {0} debe tener tener como máximo {1} caracteres.")]
        [DataType(DataType.EmailAddress)]
        public string EMail { get; set; }

        [DataMember]
        [Display(Name = "Dirección")]
        [StringLength(250, ErrorMessage = "El campo {0} debe tener tener como máximo {1} caracteres.")]
        public string Direccion { get; set; }

        [DataMember]
        public virtual PuestoTrabajo PuestoTrabajo { get; set; }

        public virtual Cliente Cliente { get; set; }
    }
}