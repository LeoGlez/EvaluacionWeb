﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EvaluacionWeb.Models
{
    [DataContract]
    public class Cliente
    {
        [DataMember]
        [Key]
        public int IdCliente { get; set; }

        [DataMember]
        [Required(ErrorMessage = "Debes ingresar el tipo de persona.")]
        public string IdTipoPersona { get; set; }

        [DataMember]
        [Display(Name = "Razon Social")]
        [Required(ErrorMessage = "Debes ingresar {0}.")]
        [StringLength(150, ErrorMessage = "El campo {0} debe tener tener como máximo {1} caracteres.")]
        public string RazonSocial { get; set; }

        [DataMember]
        [Display(Name = "Nombre Comercial")]
        [Required(ErrorMessage = "Debes ingresar {0}.")]
        [StringLength(150, ErrorMessage = "El campo {0} debe tener tener como máximo {1} caracteres.")]
        public string NombreComercial { get; set; }

        [DataMember]
        [Display(Name = "RFC")]
        [Required(ErrorMessage = "Debes ingresar {0}.")]
        [StringLength(15, ErrorMessage = "El campo {0} debe tener tener como máximo {1} caracteres.")]
        public string RFC { get; set; }

        [DataMember]
        [Display(Name = "CURP")]
        [StringLength(20, ErrorMessage = "El campo {0} debe tener tener como máximo {1} caracteres.")]
        public string CURP { get; set; }

        [DataMember]
        [Display(Name = "Dirección")]
        [StringLength(250, ErrorMessage = "El campo {0} debe tener tener como máximo {1} caracteres.")]
        public string Direccion { get; set; }

        [DataMember]
        public virtual TipoPersona TipoPersona { get; set; }
        [DataMember]
        public virtual ICollection<Contacto> Contactos { get; set; }
    }
}