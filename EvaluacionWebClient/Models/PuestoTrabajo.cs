﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace EvaluacionWeb.Models
{
    [DataContract]
    public class PuestoTrabajo
    {
        [DataMember]
        [Key]
        public int IdPuestoTrabajo { get; set; }

        [DataMember]
        public string Descripcion { get; set; }

        public virtual ICollection<Contacto> Contactos { get; set; }
    }
}