﻿using EvaluacionWeb.Models;
using EvaluacionWebMVC.DefaultSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace EvaluacionWebClient.Controllers
{
    public class ClientsController : Controller
    {
        // GET: Clients
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetClients()
        {
            IEnumerable<Cliente> Clients = new List<Cliente>();
            HttpResponseMessage Response = WebServerInformation.HttpClient.GetAsync("Clientes").Result;
            Clients = Response.Content.ReadAsAsync<IEnumerable<Cliente>>().Result;

            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            var model = (Clients.ToList()
                        .Select(x => new
                        {
                            idCliente = x.IdCliente,
                            idTipoPersona = x.IdTipoPersona,
                            razonSocial = x.RazonSocial,
                            nombreComercial = x.NombreComercial,
                            rfc = x.RFC,
                            curp = x.CURP,
                            direccion = x.Direccion
                        })).ToList();

            return Json(new
            {
                draw = draw,
                recordsFiltered = model.Count,
                recordsTotal = model.Count,
                data = model
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveClient(Cliente Cliente)
        {
            Cliente NuevoCliente = new Cliente();

            if (Cliente.Contactos == null)
            {
                Cliente.Contactos = new List<Contacto>();
            }

            if (Cliente.TipoPersona == null)
            {
                Cliente.TipoPersona = new TipoPersona();
            }

            if (Cliente.IdCliente > 0)
            {
                HttpResponseMessage Response = WebServerInformation.HttpClient.PutAsJsonAsync("Clientes/" + Cliente.IdCliente.ToString(), Cliente).Result;
                NuevoCliente = Response.Content.ReadAsAsync<Cliente>().Result;
            }
            else
            {
                HttpResponseMessage Response = WebServerInformation.HttpClient.PostAsJsonAsync("Clientes", Cliente).Result;
                NuevoCliente = Response.Content.ReadAsAsync<Cliente>().Result;
            }           

            if (!(NuevoCliente.IdCliente > 0))
            {
                return Json(new { error = true, message = "No se han podido guardar los datos del cliente." });
            }

            return Json(new { error = false, message = "Cliente guardado satisfactoriamente!" });

        }

        public ActionResult GetSingleClient(int? clientId)
        {
            Cliente ClienteConsultado = new Cliente();
            HttpResponseMessage Response = WebServerInformation.HttpClient.GetAsync("Clientes/" + clientId.ToString()).Result;
            ClienteConsultado = Response.Content.ReadAsAsync<Cliente>().Result;

            return Json(new { result = ClienteConsultado }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetPersonTypes()
        {
            IEnumerable<TipoPersona> PersonTypes = new List<TipoPersona>();
            PersonTypes = WebServerInformation.PersonTypesForLists;

            var model = (PersonTypes.ToList()
                        .Select(x => new
                        {
                            idTipoPersona = x.IdTipoPersona,
                            descripcion = x.Descripcion
                        })).ToList();

            return Json(model, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetJobs()
        {
            IEnumerable<PuestoTrabajo> Jobs = new List<PuestoTrabajo>();
            Jobs = WebServerInformation.JobsForLists;

            var model = (Jobs.ToList()
                        .Select(x => new
                        {
                            idPuestoTrabajo = x.IdPuestoTrabajo,
                            descripcion = x.Descripcion
                        })).ToList();

            return Json(model, JsonRequestBehavior.AllowGet);

        }

        // GET: Clients/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Clients/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clients/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Clients/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Clients/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Clients/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Clients/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
