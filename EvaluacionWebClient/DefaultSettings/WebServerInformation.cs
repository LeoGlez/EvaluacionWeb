﻿using EvaluacionWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace EvaluacionWebMVC.DefaultSettings
{
    public static class WebServerInformation
    {
        public static HttpClient HttpClient = new HttpClient();
        public static List<TipoPersona> PersonTypesForLists = new List<TipoPersona>();
        public static List<PuestoTrabajo> JobsForLists = new List<PuestoTrabajo>();

        static WebServerInformation()
        {
            HttpClient.BaseAddress = new Uri("http://localhost:54617/api/");
            HttpClient.DefaultRequestHeaders.Accept.Clear();
            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            PersonTypesForLists = GetPersonTypesForLists();

            JobsForLists = GetJobsForLists();

        }

        private static List<TipoPersona> GetPersonTypesForLists()
        {
            IEnumerable<TipoPersona> TiposPersona;
            HttpResponseMessage Response = HttpClient.GetAsync("TiposPersona").Result;
            TiposPersona = Response.Content.ReadAsAsync<IEnumerable<TipoPersona>>().Result;

            var list = TiposPersona.ToList();
            list.Add(new TipoPersona { IdTipoPersona = "", Descripcion = "[Tipo de Persona...]" });
            list = list.OrderBy(p => p.Descripcion).ToList();

            return list;
        }

        private static List<PuestoTrabajo> GetJobsForLists()
        {
            IEnumerable<PuestoTrabajo> PuestosTrabajo;
            HttpResponseMessage Response = HttpClient.GetAsync("PuestosTrabajo").Result;
            PuestosTrabajo = Response.Content.ReadAsAsync<IEnumerable<PuestoTrabajo>>().Result;

            var list = PuestosTrabajo.ToList();
            list.Add(new PuestoTrabajo { IdPuestoTrabajo = 0, Descripcion = "[Puesto...]" });
            list = list.OrderBy(p => p.Descripcion).ToList();

            return list;
        }

    }
}