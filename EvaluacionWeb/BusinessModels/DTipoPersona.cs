﻿using EvaluacionWeb.DataAccessModels;
using EvaluacionWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EvaluacionWeb.BusinessModels
{
    public class DTipoPersona
    {
        private ADTipoPersona ADTipoPersona = new ADTipoPersona();

        public List<TipoPersona> ConsultarTodos()
        {
            return ADTipoPersona.ConsultarTodos();
        }

        public TipoPersona Consultar(string id)
        {
            return ADTipoPersona.Consultar(id);
        }
    }
}