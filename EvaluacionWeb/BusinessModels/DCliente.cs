﻿using EvaluacionWeb.DataAccessModels;
using EvaluacionWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EvaluacionWeb.BusinessModels
{
    public class DCliente
    {
        private ADCliente ADCliente = new ADCliente();

        public List<Cliente> ConsultarTodos()
        {
            return ADCliente.ConsultarTodos();
        }

        public Cliente Consultar(int IdCliente)
        {
            return ADCliente.Consultar(IdCliente);
        }

        public Cliente Crear(Cliente Cliente)
        {
            return ADCliente.Crear(Cliente);
        }

        public Cliente Actualizar(Cliente Cliente)
        {
            return ADCliente.Actualizar(Cliente);
        }

        public Cliente Eliminar(Cliente Cliente)
        {
            return ADCliente.Eliminar(Cliente);
        }
    }
}