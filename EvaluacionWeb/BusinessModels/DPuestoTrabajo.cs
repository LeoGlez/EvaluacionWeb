﻿using EvaluacionWeb.DataAccessModels;
using EvaluacionWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EvaluacionWeb.BusinessModels
{
    public class DPuestoTrabajo
    {
        private ADPuestoTrabajo ADPuestoTrabajo = new ADPuestoTrabajo();

        public List<PuestoTrabajo> ConsultarTodos()
        {
            return ADPuestoTrabajo.ConsultarTodos();
        }

        public PuestoTrabajo Consultar(int id)
        {
            return ADPuestoTrabajo.Consultar(id);
        }
    }
}