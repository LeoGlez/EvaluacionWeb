﻿using EvaluacionWeb.BusinessModels;
using EvaluacionWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace EvaluacionWeb.Controllers
{
    public class ClientesController : ApiController
    {
        // GET: api/Cliente
        public IHttpActionResult GetCliente()
        {
            DCliente DCliente = new DCliente();
            List<Cliente> Clientes = new List<Cliente>();
            Clientes = DCliente.ConsultarTodos();

            return Ok(Clientes);
        }

        // GET: api/Cliente/5
        public IHttpActionResult GetCliente(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest("Debe indicar un Id de Cliente.");
            }

            if (!(id > 0))
            {
                return BadRequest("El Id de Cliente debe ser mayor a cero.");
            }

            DCliente DCliente = new DCliente();
            Cliente Cliente = new Cliente();
            Cliente = DCliente.Consultar(Convert.ToInt32(id));

            if (!(Cliente.IdCliente > 0))
            {
                return BadRequest("El cliente no existe.");
            }

            return Ok(Cliente);
        }

        // POST: api/Cliente
        [ResponseType(typeof(Cliente))]
        public IHttpActionResult PostCliente(Cliente cliente)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DCliente DCliente = new DCliente();
            Cliente nuevoCliente = DCliente.Crear(cliente);

            if (!(nuevoCliente.IdCliente > 0))
            {
                return BadRequest("No se han podido guardar los datos del cliente.");
            }

            return Ok(nuevoCliente);

        }

        // PUT: api/Cliente/5
        [ResponseType(typeof(Cliente))]
        public IHttpActionResult PutCliente(int? id, Cliente cliente)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!id.HasValue)
            {
                return BadRequest("Debe indicar un Id de Cliente.");
            }

            if (!(id > 0))
            {
                return BadRequest("El Id de Cliente debe ser mayor a cero.");
            }

            if (id != cliente.IdCliente)
            {
                return BadRequest("Los Id de Cliente no coinciden.");
            }

            DCliente DCliente = new DCliente();
            Cliente Cliente = new Cliente();
            Cliente = DCliente.Consultar(Convert.ToInt32(id));

            if (!(Cliente.IdCliente > 0))
            {
                return BadRequest("El cliente no existe.");
            }

            Cliente = new Cliente();
            Cliente = DCliente.Actualizar(cliente);

            if (!(Cliente.IdCliente > 0))
            {
                return BadRequest("No se han podido actualizar los datos del cliente.");
            }

            return Ok(Cliente);
        }

        // DELETE: api/Cliente/5
        [ResponseType(typeof(Cliente))]
        public IHttpActionResult DeleteCliente(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest("Debe indicar un Id de Cliente.");
            }

            if (!(id > 0))
            {
                return BadRequest("El Id de Cliente debe ser mayor a cero.");
            }

            DCliente DCliente = new DCliente();
            Cliente Cliente = new Cliente();
            Cliente = DCliente.Consultar(Convert.ToInt32(id));

            if (!(Cliente.IdCliente > 0))
            {
                return BadRequest("El cliente no existe.");
            }

            Cliente ClienteEliminado = new Cliente();
            ClienteEliminado = DCliente.Eliminar(Cliente);

            if (!(ClienteEliminado.IdCliente > 0))
            {
                return BadRequest("No se pudieron eliminar los datos del cliente.");
            }

            return Ok(ClienteEliminado);
        }
    }
}
