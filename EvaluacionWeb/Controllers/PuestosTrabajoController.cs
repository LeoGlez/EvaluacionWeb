﻿using EvaluacionWeb.BusinessModels;
using EvaluacionWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EvaluacionWeb.Controllers
{
    public class PuestosTrabajoController : ApiController
    {
        // GET: api/PuestosTrabajo
        public IHttpActionResult GetPuestoTrabajo()
        {
            DPuestoTrabajo DPuestoTrabajo = new DPuestoTrabajo();
            List<PuestoTrabajo> PuestosTrabajo = new List<PuestoTrabajo>();
            PuestosTrabajo = DPuestoTrabajo.ConsultarTodos();

            return Ok(PuestosTrabajo);
        }

        // GET: api/PuestosTrabajo/5
        public IHttpActionResult GetPuestoTrabajo(int? id)
        {
            if (!id.HasValue)
            {
                return BadRequest("Debe indicar el Id del Puesto.");
            }

            if (!(id > 0))
            {
                return BadRequest("El Id del Puesto debe ser mayor a cero.");
            }

            DPuestoTrabajo DPuestoTrabajo = new DPuestoTrabajo();
            PuestoTrabajo PuestoTrabajo = new PuestoTrabajo();
            PuestoTrabajo = DPuestoTrabajo.Consultar(Convert.ToInt32(id));

            if (!(PuestoTrabajo.IdPuestoTrabajo > 0))
            {
                return BadRequest("El puesto no existe.");
            }

            return Ok(PuestoTrabajo);
        }
    }
}
