﻿using EvaluacionWeb.BusinessModels;
using EvaluacionWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EvaluacionWeb.Controllers
{
    public class TiposPersonaController : ApiController
    {
        // GET: api/TiposPersona
        public IHttpActionResult GetTipoPersona()
        {
            DTipoPersona DTipoPersona = new DTipoPersona();
            List<TipoPersona> TiposPersona = new List<TipoPersona>();
            TiposPersona = DTipoPersona.ConsultarTodos();

            return Ok(TiposPersona);
        }

        // GET: api/TiposPersona/F
        public IHttpActionResult GetTipoPersona(string id)
        {
            if (!(id != null))
            {
                return BadRequest("Debe indicar el Id del Tipo de Persona.");
            }

            if (!(id != ""))
            {
                return BadRequest("El Id del Tipo de Persona no debe estar vacío..");
            }

            DTipoPersona DTipoPersona = new DTipoPersona();
            TipoPersona TipoPersona = new TipoPersona();
            TipoPersona = DTipoPersona.Consultar(id);

            if (!(TipoPersona.IdTipoPersona != ""))
            {
                return BadRequest("El tipo de persona no existe.");
            }

            return Ok(TipoPersona);
        }
    }
}
