﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EvaluacionWeb.Libraries
{
    public enum AccionesCliente : byte
    {
        ConsultarTodos,
        Consultar,
        Crear,
        Modificar,
        Eliminar
    }
}