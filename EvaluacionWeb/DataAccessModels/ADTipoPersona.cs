﻿using EvaluacionWeb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace EvaluacionWeb.DataAccessModels
{
    public class ADTipoPersona
    {
        public List<TipoPersona> ConsultarTodos()
        {
            List<TipoPersona> TiposPersona = new List<TipoPersona>();

            try
            {
                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["EvaluationDB"].ToString()))
                {

                    conexion.Open();

                    SqlCommand comando;
                    comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = "SELECT IdTipoPersona, Descripcion FROM TiposPersona";

                    using (SqlDataReader lectorDatos = comando.ExecuteReader())
                    {

                        while (lectorDatos.Read())
                        {
                            TipoPersona TipoPersona = new TipoPersona
                            {
                                IdTipoPersona = lectorDatos["IdTipoPersona"].ToString(),
                                Descripcion = lectorDatos["Descripcion"].ToString()
                            };

                            TiposPersona.Add(TipoPersona);
                        }

                    }

                }

            }
            catch (Exception)
            {
                TiposPersona = new List<TipoPersona>();
                //throw;
            }

            return TiposPersona;
        }

        public TipoPersona Consultar(string IdTipoPersona)
        {
            TipoPersona TipoPersona = new TipoPersona();

            try
            {

                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["EvaluationDB"].ToString()))
                {

                    conexion.Open();

                    SqlCommand comando;
                    comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = "SELECT IdTipoPersona, Descripcion FROM TiposPersona WHERE IdTipoPersona = @ID_TIPO_PERSONA";
                    comando.Parameters.AddWithValue("@ID_TIPO_PERSONA", IdTipoPersona);

                    using (SqlDataReader lectorDatos = comando.ExecuteReader())
                    {

                        if (lectorDatos.HasRows)
                        {
                            lectorDatos.Read();
                            TipoPersona = new TipoPersona
                            {
                                IdTipoPersona = lectorDatos["IdTipoPersona"].ToString(),
                                Descripcion = lectorDatos["Descripcion"].ToString()
                            };
                        }

                    }

                }

            }
            catch (Exception)
            {
                TipoPersona = new TipoPersona();
                //throw;
            }

            return TipoPersona;
        }
    }
}