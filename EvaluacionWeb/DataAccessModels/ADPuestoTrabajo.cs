﻿using EvaluacionWeb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace EvaluacionWeb.DataAccessModels
{
    public class ADPuestoTrabajo
    {
        public List<PuestoTrabajo> ConsultarTodos()
        {
            List<PuestoTrabajo> Puestos = new List<PuestoTrabajo>();

            try
            {
                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["EvaluationDB"].ToString()))
                {

                    conexion.Open();

                    SqlCommand comando;
                    comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = "SELECT IdPuestoTrabajo, Descripcion FROM PuestosTrabajo";

                    using (SqlDataReader lectorDatos = comando.ExecuteReader())
                    {

                        while (lectorDatos.Read())
                        {
                            PuestoTrabajo PuestoTrabajo = new PuestoTrabajo {
                                IdPuestoTrabajo = Convert.ToInt32(lectorDatos["IdPuestoTrabajo"]),
                                Descripcion = lectorDatos["Descripcion"].ToString()
                            };

                            Puestos.Add(PuestoTrabajo);
                        }

                    }

                }

            }
            catch (Exception)
            {
                Puestos = new List<PuestoTrabajo>();
                //throw;
            }

            return Puestos;
        }

        public PuestoTrabajo Consultar(int IdPuestoTrabajo)
        {
            PuestoTrabajo Puesto = new PuestoTrabajo();

            try
            {

                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["EvaluationDB"].ToString()))
                {

                    conexion.Open();

                    SqlCommand comando;
                    comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = "SELECT IdPuestoTrabajo, Descripcion FROM PuestosTrabajo WHERE IdPuestoTrabajo = @ID_PUESTO_TRABAJO";
                    comando.Parameters.AddWithValue("@ID_PUESTO_TRABAJO", IdPuestoTrabajo);

                    using (SqlDataReader lectorDatos = comando.ExecuteReader())
                    {

                        if (lectorDatos.HasRows)
                        {
                            lectorDatos.Read();
                            Puesto = new PuestoTrabajo
                            {
                                IdPuestoTrabajo = Convert.ToInt32(lectorDatos["IdPuestoTrabajo"]),
                                Descripcion = lectorDatos["Descripcion"].ToString()
                            };
                        }

                    }

                }

            }
            catch (Exception)
            {
                Puesto = new PuestoTrabajo();
                //throw;
            }

            return Puesto;
        }

    }
}