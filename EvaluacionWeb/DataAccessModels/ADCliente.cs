﻿using EvaluacionWeb.Libraries;
using EvaluacionWeb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace EvaluacionWeb.DataAccessModels
{
    public class ADCliente
    {
        public List<Cliente> ConsultarTodos()
        {
            List<Cliente> Clientes = new List<Cliente>();

            try
            {
                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["EvaluationDB"].ToString())) {

                    conexion.Open();

                    SqlCommand comando;
                    comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = "EXEC SP_CLIENTES @ACCION, @ID_CLIENTE, @ID_TIPO_PERSONA, @RAZON_SOCIAL, @NOMBRE_COMERCIAL, @RFC, @CURP, @DIRECCION ";
                    comando.Parameters.AddWithValue("@ACCION", Convert.ToInt32(AccionesCliente.ConsultarTodos));
                    comando.Parameters.AddWithValue("@ID_CLIENTE", "0");
                    comando.Parameters.AddWithValue("@ID_TIPO_PERSONA", "");
                    comando.Parameters.AddWithValue("@RAZON_SOCIAL", "");
                    comando.Parameters.AddWithValue("@NOMBRE_COMERCIAL", "");
                    comando.Parameters.AddWithValue("@RFC", "");
                    comando.Parameters.AddWithValue("@CURP", "");
                    comando.Parameters.AddWithValue("@DIRECCION", "");

                    using (SqlDataReader lectorDatos = comando.ExecuteReader()) {

                        while (lectorDatos.Read())
                        {
                            Cliente clienteActual = new Cliente
                            {
                                IdCliente = Convert.ToInt32(lectorDatos["IdCliente"]),
                                IdTipoPersona = lectorDatos["IdTipoPersona"].ToString(),
                                RazonSocial = lectorDatos["RazonSocial"].ToString(),
                                NombreComercial = lectorDatos["NombreComercial"].ToString(),
                                RFC = lectorDatos["RFC"].ToString(),
                                CURP = lectorDatos["CURP"].ToString(),
                                Direccion = lectorDatos["Direccion"].ToString(),
                                TipoPersona = new TipoPersona
                                {
                                    IdTipoPersona = lectorDatos["IdTipoPersona"].ToString(),
                                    Descripcion = lectorDatos["TipoPersona"].ToString()
                                },
                                Contactos = new List<Contacto>()
                            };

                            Clientes.Add(clienteActual);
                        }

                    }


                    foreach (var item in Clientes)
                    {
                        comando = new SqlCommand();
                        comando.Connection = conexion;
                        comando.CommandText = "EXEC SP_CONTACTOS @ACCION, @ID_CONTACTO, @ID_CLIENTE, @NOMBRE, @APELLIDOP, @APELLIDOM, @ID_PUESTO, @TELEFONO, @EMAIL, @DIRECCION";
                        comando.Parameters.AddWithValue("@ACCION", Convert.ToInt32(AccionesContacto.ConsultarPorCliente));
                        comando.Parameters.AddWithValue("@ID_CONTACTO", "0");
                        comando.Parameters.AddWithValue("@ID_CLIENTE", item.IdCliente);
                        comando.Parameters.AddWithValue("@NOMBRE", "");
                        comando.Parameters.AddWithValue("@APELLIDOP", "");
                        comando.Parameters.AddWithValue("@APELLIDOM", "");
                        comando.Parameters.AddWithValue("@ID_PUESTO", "");
                        comando.Parameters.AddWithValue("@TELEFONO", "");
                        comando.Parameters.AddWithValue("@EMAIL", "");
                        comando.Parameters.AddWithValue("@DIRECCION", "");

                        using (SqlDataReader lectorDatos = comando.ExecuteReader()) {

                            while (lectorDatos.Read())
                            {
                                Contacto contactoActual = new Contacto
                                {
                                    IdContacto = Convert.ToInt32(lectorDatos["IdContacto"]),
                                    IdCliente = Convert.ToInt32(lectorDatos["IdCliente"]),
                                    Nombre = lectorDatos["Nombre"].ToString(),
                                    ApellidoP = lectorDatos["ApellidoP"].ToString(),
                                    ApellidoM = lectorDatos["ApellidoM"].ToString(),
                                    IdPuestoTrabajo = Convert.ToInt32(lectorDatos["IdPuestoTrabajo"]),
                                    Telefono = lectorDatos["Telefono"].ToString(),
                                    EMail = lectorDatos["EMail"].ToString(),
                                    Direccion = lectorDatos["Direccion"].ToString(),
                                    PuestoTrabajo = new PuestoTrabajo
                                    {
                                        IdPuestoTrabajo = Convert.ToInt32(lectorDatos["IdPuestoTrabajo"]),
                                        Descripcion = lectorDatos["PuestoTrabajo"].ToString()
                                    }
                                };

                                item.Contactos.Add(contactoActual);

                            }

                        }

                    }                  

                }
                                            
            }
            catch (Exception)
            {
                Clientes = new List<Cliente>();
                //throw;
            }
            
            return Clientes;
        }

        public Cliente Consultar(int IdCliente)
        {
            Cliente Cliente = new Cliente();

            try
            {

                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["EvaluationDB"].ToString())) {

                    conexion.Open();

                    SqlCommand comando;
                    comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = "EXEC SP_CLIENTES @ACCION, @ID_CLIENTE, @ID_TIPO_PERSONA, @RAZON_SOCIAL, @NOMBRE_COMERCIAL, @RFC, @CURP, @DIRECCION ";
                    comando.Parameters.AddWithValue("@ACCION", Convert.ToInt32(AccionesCliente.Consultar));
                    comando.Parameters.AddWithValue("@ID_CLIENTE", IdCliente);
                    comando.Parameters.AddWithValue("@ID_TIPO_PERSONA", "");
                    comando.Parameters.AddWithValue("@RAZON_SOCIAL", "");
                    comando.Parameters.AddWithValue("@NOMBRE_COMERCIAL", "");
                    comando.Parameters.AddWithValue("@RFC", "");
                    comando.Parameters.AddWithValue("@CURP", "");
                    comando.Parameters.AddWithValue("@DIRECCION", "");

                    using (SqlDataReader lectorDatos = comando.ExecuteReader()) {

                        if (lectorDatos.HasRows)
                        {
                            lectorDatos.Read();
                            Cliente = new Cliente {
                                IdCliente = Convert.ToInt32(lectorDatos["IdCliente"]),
                                IdTipoPersona = lectorDatos["IdTipoPersona"].ToString(),
                                RazonSocial = lectorDatos["RazonSocial"].ToString(),
                                NombreComercial = lectorDatos["NombreComercial"].ToString(),
                                RFC = lectorDatos["RFC"].ToString(),
                                CURP = lectorDatos["CURP"].ToString(),
                                Direccion = lectorDatos["Direccion"].ToString(),
                                TipoPersona = new TipoPersona
                                {
                                    IdTipoPersona = lectorDatos["IdTipoPersona"].ToString(),
                                    Descripcion = lectorDatos["TipoPersona"].ToString()
                                },
                                Contactos = new List<Contacto>()
                            };
                        }

                    }

                    if (Cliente.IdCliente > 0)
                    {

                        comando = new SqlCommand();
                        comando.Connection = conexion;
                        comando.CommandText = "EXEC SP_CONTACTOS @ACCION, @ID_CONTACTO, @ID_CLIENTE, @NOMBRE, @APELLIDOP, @APELLIDOM, @ID_PUESTO, @TELEFONO, @EMAIL, @DIRECCION";
                        comando.Parameters.AddWithValue("@ACCION", Convert.ToInt32(AccionesContacto.ConsultarPorCliente));
                        comando.Parameters.AddWithValue("@ID_CONTACTO", "0");
                        comando.Parameters.AddWithValue("@ID_CLIENTE", Cliente.IdCliente);
                        comando.Parameters.AddWithValue("@NOMBRE", "");
                        comando.Parameters.AddWithValue("@APELLIDOP", "");
                        comando.Parameters.AddWithValue("@APELLIDOM", "");
                        comando.Parameters.AddWithValue("@ID_PUESTO", "");
                        comando.Parameters.AddWithValue("@TELEFONO", "");
                        comando.Parameters.AddWithValue("@EMAIL", "");
                        comando.Parameters.AddWithValue("@DIRECCION", "");

                        using (SqlDataReader lectorDatos = comando.ExecuteReader())
                        {

                            while (lectorDatos.Read())
                            {
                                Contacto contactoActual = new Contacto
                                {
                                    IdContacto = Convert.ToInt32(lectorDatos["IdContacto"]),
                                    IdCliente = Convert.ToInt32(lectorDatos["IdCliente"]),
                                    Nombre = lectorDatos["Nombre"].ToString(),
                                    ApellidoP = lectorDatos["ApellidoP"].ToString(),
                                    ApellidoM = lectorDatos["ApellidoM"].ToString(),
                                    IdPuestoTrabajo = Convert.ToInt32(lectorDatos["IdPuestoTrabajo"]),
                                    Telefono = lectorDatos["Telefono"].ToString(),
                                    EMail = lectorDatos["EMail"].ToString(),
                                    Direccion = lectorDatos["Direccion"].ToString(),
                                    PuestoTrabajo = new PuestoTrabajo
                                    {
                                        IdPuestoTrabajo = Convert.ToInt32(lectorDatos["IdPuestoTrabajo"]),
                                        Descripcion = lectorDatos["PuestoTrabajo"].ToString()
                                    }
                                };

                                Cliente.Contactos.Add(contactoActual);

                            }

                        }

                    }
                    

                }

            }
            catch (Exception)
            {
                Cliente = new Cliente();
                //throw;
            }

            return Cliente;
        }

        public Cliente Crear(Cliente Cliente) {

            Cliente nuevoCliente = new Cliente();

            using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["EvaluationDB"].ToString()))
            {
                conexion.Open();

                SqlTransaction transaccion;
                transaccion = conexion.BeginTransaction();

                SqlCommand comando;

                try
                {                    
                    comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = "EXEC SP_CLIENTES @ACCION, @ID_CLIENTE, @ID_TIPO_PERSONA, @RAZON_SOCIAL, @NOMBRE_COMERCIAL, @RFC, @CURP, @DIRECCION ";
                    comando.Parameters.AddWithValue("@ACCION", Convert.ToInt32(AccionesCliente.Crear));
                    comando.Parameters.AddWithValue("@ID_CLIENTE", Cliente.IdCliente);
                    comando.Parameters.AddWithValue("@ID_TIPO_PERSONA", Cliente.IdTipoPersona);
                    comando.Parameters.AddWithValue("@RAZON_SOCIAL", Cliente.RazonSocial);
                    comando.Parameters.AddWithValue("@NOMBRE_COMERCIAL", Cliente.NombreComercial);
                    comando.Parameters.AddWithValue("@RFC", Cliente.RFC);
                    comando.Parameters.AddWithValue("@CURP", Cliente.CURP);
                    comando.Parameters.AddWithValue("@DIRECCION", Cliente.Direccion);
                    comando.Transaction = transaccion;

                    using (SqlDataReader lectorDatos = comando.ExecuteReader())
                    {
                        if (lectorDatos.HasRows)
                        {
                            lectorDatos.Read();
                            nuevoCliente = new Cliente {
                                IdCliente = Convert.ToInt32(lectorDatos["IdCliente"]),
                                IdTipoPersona = lectorDatos["IdTipoPersona"].ToString(),
                                RazonSocial = lectorDatos["RazonSocial"].ToString(),
                                NombreComercial = lectorDatos["NombreComercial"].ToString(),
                                RFC = lectorDatos["RFC"].ToString(),
                                CURP = lectorDatos["CURP"].ToString(),
                                Direccion = lectorDatos["Direccion"].ToString(),
                                TipoPersona = new TipoPersona
                                {
                                    IdTipoPersona = lectorDatos["IdTipoPersona"].ToString(),
                                    Descripcion = lectorDatos["TipoPersona"].ToString()
                                },
                                Contactos = new List<Contacto>()
                            };
                        }
                    }

                    foreach (var item in Cliente.Contactos)
                    {
                        item.IdCliente = nuevoCliente.IdCliente;
                        comando = new SqlCommand();
                        comando.Connection = conexion;
                        comando.CommandText = "EXEC SP_CONTACTOS @ACCION, @ID_CONTACTO, @ID_CLIENTE, @NOMBRE, @APELLIDOP, @APELLIDOM, @ID_PUESTO, @TELEFONO, @EMAIL, @DIRECCION";
                        comando.Parameters.AddWithValue("@ACCION", Convert.ToInt32(AccionesContacto.Crear));
                        comando.Parameters.AddWithValue("@ID_CONTACTO", item.IdContacto);
                        comando.Parameters.AddWithValue("@ID_CLIENTE", item.IdCliente);
                        comando.Parameters.AddWithValue("@NOMBRE", item.Nombre);
                        comando.Parameters.AddWithValue("@APELLIDOP", item.ApellidoP);
                        comando.Parameters.AddWithValue("@APELLIDOM", item.ApellidoM);
                        comando.Parameters.AddWithValue("@ID_PUESTO", item.IdPuestoTrabajo);
                        comando.Parameters.AddWithValue("@TELEFONO", item.Telefono);
                        comando.Parameters.AddWithValue("@EMAIL", item.EMail);
                        comando.Parameters.AddWithValue("@DIRECCION", item.Direccion);
                        comando.Transaction = transaccion;

                        using (SqlDataReader lectorDatos = comando.ExecuteReader())
                        {
                            if (lectorDatos.HasRows)
                            {
                                lectorDatos.Read();
                                nuevoCliente.Contactos.Add(
                                    new Contacto {
                                        IdContacto = Convert.ToInt32(lectorDatos["IdContacto"]),
                                        IdCliente = Convert.ToInt32(lectorDatos["IdCliente"]),
                                        Nombre = lectorDatos["Nombre"].ToString(),
                                        ApellidoP = lectorDatos["ApellidoP"].ToString(),
                                        ApellidoM = lectorDatos["ApellidoM"].ToString(),
                                        IdPuestoTrabajo = Convert.ToInt32(lectorDatos["IdPuestoTrabajo"]),
                                        Telefono = lectorDatos["Telefono"].ToString(),
                                        EMail = lectorDatos["EMail"].ToString(),
                                        Direccion = lectorDatos["Direccion"].ToString(),
                                        PuestoTrabajo = new PuestoTrabajo
                                        {
                                            IdPuestoTrabajo = Convert.ToInt32(lectorDatos["IdPuestoTrabajo"]),
                                            Descripcion = lectorDatos["PuestoTrabajo"].ToString()
                                        }
                                    }
                                );
                            }
                        }

                    }

                    transaccion.Commit();

                }
                catch (Exception)
                {
                    transaccion.Rollback();
                    nuevoCliente = new Cliente();
                    //throw;
                }

            }
            
            return nuevoCliente;
        }

        public Cliente Actualizar(Cliente Cliente) {

            Cliente clienteActualizado = new Cliente();
            List<Contacto> contactos = Consultar(Cliente.IdCliente).Contactos.ToList();

            using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["EvaluationDB"].ToString()))
            {

                conexion.Open();

                SqlTransaction transaccion;
                transaccion = conexion.BeginTransaction();

                SqlCommand comando;

                try
                {
                    comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = "EXEC SP_CLIENTES @ACCION, @ID_CLIENTE, @ID_TIPO_PERSONA, @RAZON_SOCIAL, @NOMBRE_COMERCIAL, @RFC, @CURP, @DIRECCION ";
                    comando.Parameters.AddWithValue("@ACCION", Convert.ToInt32(AccionesCliente.Modificar));
                    comando.Parameters.AddWithValue("@ID_CLIENTE", Cliente.IdCliente);
                    comando.Parameters.AddWithValue("@ID_TIPO_PERSONA", Cliente.IdTipoPersona);
                    comando.Parameters.AddWithValue("@RAZON_SOCIAL", Cliente.RazonSocial);
                    comando.Parameters.AddWithValue("@NOMBRE_COMERCIAL", Cliente.NombreComercial);
                    comando.Parameters.AddWithValue("@RFC", Cliente.RFC);
                    comando.Parameters.AddWithValue("@CURP", Cliente.CURP);
                    comando.Parameters.AddWithValue("@DIRECCION", Cliente.Direccion);
                    comando.Transaction = transaccion;

                    using (SqlDataReader lectorDatos = comando.ExecuteReader())
                    {
                        if (lectorDatos.HasRows)
                        {
                            lectorDatos.Read();
                            clienteActualizado = new Cliente {
                                IdCliente = Convert.ToInt32(lectorDatos["IdCliente"]),
                                IdTipoPersona = lectorDatos["IdTipoPersona"].ToString(),
                                RazonSocial = lectorDatos["RazonSocial"].ToString(),
                                NombreComercial = lectorDatos["NombreComercial"].ToString(),
                                RFC = lectorDatos["RFC"].ToString(),
                                CURP = lectorDatos["CURP"].ToString(),
                                Direccion = lectorDatos["Direccion"].ToString(),
                                TipoPersona = new TipoPersona
                                {
                                    IdTipoPersona = lectorDatos["IdTipoPersona"].ToString(),
                                    Descripcion = lectorDatos["TipoPersona"].ToString()
                                },
                                Contactos = new List<Contacto>()
                            };
                        }
                    }

                    foreach (var item in Cliente.Contactos)
                    {
                        Int32 accion;

                        if (item.IdContacto > 0)
                        {
                            accion = Convert.ToInt32(AccionesContacto.Modificar);
                        }
                        else
                        {
                            accion = Convert.ToInt32(AccionesContacto.Crear);
                        }

                        comando = new SqlCommand();
                        comando.Connection = conexion;
                        comando.CommandText = "EXEC SP_CONTACTOS @ACCION, @ID_CONTACTO, @ID_CLIENTE, @NOMBRE, @APELLIDOP, @APELLIDOM, @ID_PUESTO, @TELEFONO, @EMAIL, @DIRECCION";
                        comando.Parameters.AddWithValue("@ACCION", accion);
                        comando.Parameters.AddWithValue("@ID_CONTACTO", item.IdContacto);
                        comando.Parameters.AddWithValue("@ID_CLIENTE", Cliente.IdCliente);
                        comando.Parameters.AddWithValue("@NOMBRE", item.Nombre);
                        comando.Parameters.AddWithValue("@APELLIDOP", item.ApellidoP);
                        comando.Parameters.AddWithValue("@APELLIDOM", item.ApellidoM);
                        comando.Parameters.AddWithValue("@ID_PUESTO", item.IdPuestoTrabajo);
                        comando.Parameters.AddWithValue("@TELEFONO", item.Telefono);
                        comando.Parameters.AddWithValue("@EMAIL", item.EMail);
                        comando.Parameters.AddWithValue("@DIRECCION", item.Direccion);
                        comando.Transaction = transaccion;

                        using (SqlDataReader lectorDatos = comando.ExecuteReader())
                        {
                            if (lectorDatos.HasRows)
                            {
                                lectorDatos.Read();
                                clienteActualizado.Contactos.Add(
                                    new Contacto
                                    {
                                        IdContacto = Convert.ToInt32(lectorDatos["IdContacto"]),
                                        IdCliente = Convert.ToInt32(lectorDatos["IdCliente"]),
                                        Nombre = lectorDatos["Nombre"].ToString(),
                                        ApellidoP = lectorDatos["ApellidoP"].ToString(),
                                        ApellidoM = lectorDatos["ApellidoM"].ToString(),
                                        IdPuestoTrabajo = Convert.ToInt32(lectorDatos["IdPuestoTrabajo"]),
                                        Telefono = lectorDatos["Telefono"].ToString(),
                                        EMail = lectorDatos["EMail"].ToString(),
                                        Direccion = lectorDatos["Direccion"].ToString(),
                                        PuestoTrabajo = new PuestoTrabajo
                                        {
                                            IdPuestoTrabajo = Convert.ToInt32(lectorDatos["IdPuestoTrabajo"]),
                                            Descripcion = lectorDatos["PuestoTrabajo"].ToString()
                                        }
                                    }
                                );
                            }
                        }
                    }

                    List<Contacto> contactosCliente = Cliente.Contactos.ToList();

                    foreach (var item in contactos)
                    {
                        var clienteEncontrado = contactosCliente.Find(c => item.IdContacto == c.IdContacto);
                        if (clienteEncontrado == null)
                        {
                            comando = new SqlCommand();
                            comando.Connection = conexion;
                            comando.CommandText = "EXEC SP_CONTACTOS @ACCION, @ID_CONTACTO, @ID_CLIENTE, @NOMBRE, @APELLIDOP, @APELLIDOM, @ID_PUESTO, @TELEFONO, @EMAIL, @DIRECCION";
                            comando.Parameters.AddWithValue("@ACCION", Convert.ToInt32(AccionesContacto.Eliminar));
                            comando.Parameters.AddWithValue("@ID_CONTACTO", item.IdContacto);
                            comando.Parameters.AddWithValue("@ID_CLIENTE", item.IdCliente);
                            comando.Parameters.AddWithValue("@NOMBRE", item.Nombre);
                            comando.Parameters.AddWithValue("@APELLIDOP", item.ApellidoP);
                            comando.Parameters.AddWithValue("@APELLIDOM", item.ApellidoM);
                            comando.Parameters.AddWithValue("@ID_PUESTO", item.IdPuestoTrabajo);
                            comando.Parameters.AddWithValue("@TELEFONO", item.Telefono);
                            comando.Parameters.AddWithValue("@EMAIL", item.EMail);
                            comando.Parameters.AddWithValue("@DIRECCION", item.Direccion);
                            comando.Transaction = transaccion;

                            using (SqlDataReader lectorDatos = comando.ExecuteReader())
                            {
                                if (lectorDatos.HasRows)
                                {

                                }
                            }
                            

                        }
                    }

                    transaccion.Commit();

                }
                catch (Exception)
                {
                    transaccion.Rollback();
                    clienteActualizado = new Cliente();
                    //throw;
                }

            }

                return clienteActualizado;
        }

        public Cliente Eliminar(Cliente Cliente) {

            Cliente clienteEliminado = new Cliente();
            clienteEliminado.Contactos = new List<Contacto>();

            using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["EvaluationDB"].ToString()))
            {
                conexion.Open();

                SqlTransaction transaccion;
                transaccion = conexion.BeginTransaction();

                SqlCommand comando;

                try
                {

                    foreach (var item in Cliente.Contactos)
                    {
                        comando = new SqlCommand();
                        comando.Connection = conexion;
                        comando.CommandText = "EXEC SP_CONTACTOS @ACCION, @ID_CONTACTO, @ID_CLIENTE, @NOMBRE, @APELLIDOP, @APELLIDOM, @ID_PUESTO, @TELEFONO, @EMAIL, @DIRECCION";
                        comando.Parameters.AddWithValue("@ACCION", Convert.ToInt32(AccionesContacto.Eliminar));
                        comando.Parameters.AddWithValue("@ID_CONTACTO", item.IdContacto);
                        comando.Parameters.AddWithValue("@ID_CLIENTE", item.IdCliente);
                        comando.Parameters.AddWithValue("@NOMBRE", item.Nombre);
                        comando.Parameters.AddWithValue("@APELLIDOP", item.ApellidoP);
                        comando.Parameters.AddWithValue("@APELLIDOM", item.ApellidoM);
                        comando.Parameters.AddWithValue("@ID_PUESTO", item.IdPuestoTrabajo);
                        comando.Parameters.AddWithValue("@TELEFONO", item.Telefono);
                        comando.Parameters.AddWithValue("@EMAIL", item.EMail);
                        comando.Parameters.AddWithValue("@DIRECCION", item.Direccion);
                        comando.Transaction = transaccion;

                        using (SqlDataReader lectorDatos = comando.ExecuteReader())
                        {
                            if (lectorDatos.HasRows)
                            {
                                lectorDatos.Read();
                                clienteEliminado.Contactos.Add(
                                    new Contacto
                                    {
                                        IdContacto = Convert.ToInt32(lectorDatos["IdContacto"]),
                                        IdCliente = Convert.ToInt32(lectorDatos["IdCliente"]),
                                        Nombre = lectorDatos["Nombre"].ToString(),
                                        ApellidoP = lectorDatos["ApellidoP"].ToString(),
                                        ApellidoM = lectorDatos["ApellidoM"].ToString(),
                                        IdPuestoTrabajo = Convert.ToInt32(lectorDatos["IdPuestoTrabajo"]),
                                        Telefono = lectorDatos["Telefono"].ToString(),
                                        EMail = lectorDatos["EMail"].ToString(),
                                        Direccion = lectorDatos["Direccion"].ToString(),
                                        PuestoTrabajo = new PuestoTrabajo
                                        {
                                            IdPuestoTrabajo = Convert.ToInt32(lectorDatos["IdPuestoTrabajo"]),
                                            Descripcion = lectorDatos["PuestoTrabajo"].ToString()
                                        }
                                    }
                                );
                            }
                        }
                    }

                    comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = "EXEC SP_CLIENTES @ACCION, @ID_CLIENTE, @ID_TIPO_PERSONA, @RAZON_SOCIAL, @NOMBRE_COMERCIAL, @RFC, @CURP, @DIRECCION ";
                    comando.Parameters.AddWithValue("@ACCION", Convert.ToInt32(AccionesCliente.Eliminar));
                    comando.Parameters.AddWithValue("@ID_CLIENTE", Cliente.IdCliente);
                    comando.Parameters.AddWithValue("@ID_TIPO_PERSONA", Cliente.IdTipoPersona);
                    comando.Parameters.AddWithValue("@RAZON_SOCIAL", Cliente.RazonSocial);
                    comando.Parameters.AddWithValue("@NOMBRE_COMERCIAL", Cliente.NombreComercial);
                    comando.Parameters.AddWithValue("@RFC", Cliente.RFC);
                    comando.Parameters.AddWithValue("@CURP", Cliente.CURP);
                    comando.Parameters.AddWithValue("@DIRECCION", Cliente.Direccion);
                    comando.Transaction = transaccion;

                    using (SqlDataReader lectorDatos = comando.ExecuteReader())
                    {
                        if (lectorDatos.HasRows)
                        {
                            lectorDatos.Read();
                            Cliente = new Cliente
                            {
                                IdCliente = Convert.ToInt32(lectorDatos["IdCliente"]),
                                IdTipoPersona = lectorDatos["IdTipoPersona"].ToString(),
                                RazonSocial = lectorDatos["RazonSocial"].ToString(),
                                NombreComercial = lectorDatos["NombreComercial"].ToString(),
                                RFC = lectorDatos["RFC"].ToString(),
                                CURP = lectorDatos["CURP"].ToString(),
                                Direccion = lectorDatos["Direccion"].ToString(),
                                TipoPersona = new TipoPersona
                                {
                                    IdTipoPersona = lectorDatos["IdTipoPersona"].ToString(),
                                    Descripcion = lectorDatos["TipoPersona"].ToString()
                                },
                                Contactos = new List<Contacto>()
                            };

                            clienteEliminado.IdCliente = Cliente.IdCliente;
                            clienteEliminado.IdTipoPersona = Cliente.IdTipoPersona;
                            clienteEliminado.RazonSocial = Cliente.RazonSocial;
                            clienteEliminado.NombreComercial = Cliente.NombreComercial;
                            clienteEliminado.RFC = Cliente.RFC;
                            clienteEliminado.CURP = Cliente.CURP;
                            clienteEliminado.Direccion = Cliente.Direccion;

                        }
                    }

                    transaccion.Commit();

                }
                catch (Exception)
                {
                    transaccion.Rollback();
                    clienteEliminado = new Cliente();
                    //throw;
                }


            }

            return clienteEliminado;
        }

    }
}